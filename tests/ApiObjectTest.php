<?php

namespace OneRosterTests;

use OneRoster\ApiObject;

/**
 * @coversDefaultClass \OneRoster\ApiObject
 */
class ApiObjectTest extends TestCase
{
    public function testDeepCreate()
    {
        $data = [
            'a' => [
                'b' => [
                    'c' => 1,
                    'd' => [
                        2,
                    ],
                ],
            ],
            'w' => json_decode(json_encode([
                'x' => [
                    'y',
                    'z' => [
                        1,
                    ],
                ],
            ])),
            'q' => [
                [
                    'r' => 's',
                ],
            ],
        ];

        $obj = new ApiObject($data);

        $this->assertInstanceOf(ApiObject::class, $obj->a);
        $this->assertInstanceOf(ApiObject::class, $obj->a->b);
        $this->assertInternalType('int', $obj->a->b->c);
        $this->assertSame(1, $obj->a->b->c);
        $this->assertSame(2, $obj->a->b->d->get(0));
        $this->assertInstanceOf(ApiObject::class, $obj->q->get(0));
    }
}
