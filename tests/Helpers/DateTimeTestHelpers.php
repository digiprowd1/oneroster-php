<?php

namespace OneRosterTests\Helpers;

use Carbon\Carbon;

trait DateTimeTestHelpers
{
    /**
     * @coversNothing
     */
    public function testMakeDateTime()
    {
        $time = $this->makeDateTime(2015, 1, 2, 3, 4, 5, 678);
        $dt = Carbon::parse('2015-01-02 3:04:05.678');

        $this->assertTrue($dt->eq($time));
    }

    /**
     * @coversNothing
     */
    public function testMakeDateTimeMillisSignificant()
    {
        $first = $this->makeDateTime(2015, 1, 2, 3, 4, 5, 678);
        $second = $this->makeDateTime(2015, 1, 2, 3, 4, 5, 0);

        $this->assertFalse($first->eq($second), 'Expected time to be different with millis');
    }

    /**
     * @coversNothing
     */
    public function testMakeDateTimeSmallMillis()
    {
        $time = $this->makeDateTime(2015, 1, 2, 3, 4, 5, 1);
        $dt = Carbon::parse('2015-01-02 3:04:05.001');

        $this->assertTrue($dt->eq($time));
    }

    protected function formatDateTime($dt)
    {
        return $dt->format('Y-m-d G:i:s.u O');
    }

    protected function getSampleDateTimeParameters()
    {
        return [
            ['2016-01-02T03:04:56.789Z', $this->makeDateTime(2016, 1, 2, 3, 4, 56, 789)],
            ['2016-01-02T03:04:56.001000Z', $this->makeDateTime(2016, 1, 2, 3, 4, 56, 1)],
            ['2016-01-02T03:04:56.1Z', $this->makeDateTime(2016, 1, 2, 3, 4, 56, 100)],
            ['2016-01-02T03:04:56.0Z', $this->makeDateTime(2016, 1, 2, 3, 4, 56, 0)],
            ['2016-01-05T12:08:13.456Z', $this->makeDateTime(2016, 1, 5, 12, 8, 13, 456)],
            ['2017-10-29T01:00:02.1Z', $this->makeDateTime(2017, 10, 29, 1, 0, 2, 100)],
        ];
    }

    protected function makeDateTime($year, $month, $day, $hour, $minute, $second, $millis)
    {
        $millisString = (string) ($millis * 1000);
        $millisString = str_pad($millisString, 6, '0', STR_PAD_LEFT);

        $time = Carbon::createFromFormat('u', $millisString, 'Etc/UTC');
        $time->setDateTime($year, $month, $day, $hour, $minute, $second);

        return $time;
    }

    protected function makeDateTimeExpectationString($expected, $actual)
    {
        $parts = [
            'Expected \'' . $this->formatDateTime($expected) . '\'.',
            'Got \'' . $this->formatDateTime($expected) . '\'.',
        ];

        return implode(' ', $parts);
    }
}
