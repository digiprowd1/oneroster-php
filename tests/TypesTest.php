<?php

namespace OneRosterTests;

use OneRoster\ApiResource;
use OneRoster\Types;

class TypesTest extends TestCase
{
    /**
     * @dataProvider providerTypes
     */
    public function testTypeMatches($class, $expectedType)
    {
        $this->assertTrue(class_exists($class), 'Expected ' . $class . ' to exist.');

        $resource = new $class();

        $this->assertInstanceOf(ApiResource::class, $resource);
        $this->assertSame($expectedType, $resource->getResourceType());
    }

    public function providerTypes()
    {
        $types = Types::all();

        $parameters = [];
        foreach ($types as $type => $class) {
            $parameters[] = [$class, $type];
        }

        return $parameters;
    }
}
