<?php

namespace OneRosterTests;

use Carbon\Carbon;
use DateTime;
use OneRosterTests\Helpers\DateTimeTestHelpers;
use InvalidArgumentException;

/**
 * @coversDefaultClass \OneRoster\ApiResource
 */
class ApiResourceTest extends TestCase
{
    use DateTimeTestHelpers;

    public function providerTrait()
    {
        return $this->getSampleDateTimeParameters();
    }

    /**
     * @covers ::getDateLastModified
     * @dataProvider providerTrait
     */
    public function testGetDateLastModified($timeString, $expected)
    {
        $obj = new TestApiResource([
            'dateLastModified' => $timeString,
        ]);

        $result = $obj->getDateLastModified();

        if (!($expected instanceof DateTime)) {
            throw new InvalidArgumentException('Expected $expected to be DateTime instance');
        }

        if (!($expected instanceof Carbon)) {
            $expected = Carbon::instance($expected);
        }

        $eq = $expected->eq($result);

        $this->assertTrue(
            $eq,
            $this->makeDateTimeExpectationString($expected, $result)
        );
    }
}
