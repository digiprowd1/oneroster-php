<?php

namespace OneRosterTests\Internal;

use OneRoster\Internal\Str;
use OneRosterTests\TestCase;

/**
 * @coversDefaultClass OneRoster\Internal\Str
 */
class StrTest extends TestCase
{
    /**
     * @covers ::contains
     * @dataProvider providerTestContains
     */
    public function testContains($haystack, $needle, $expected)
    {
        $this->assertSame($expected, Str::contains($haystack, $needle));
    }

    /**
     * @covers ::endsWith
     * @dataProvider providerEndsWith
     */
    public function testEndsWith($haystack, $needle, $expected)
    {
        $this->assertSame($expected, Str::endsWith($haystack, $needle));
    }

    /**
     * @covers ::startsWith
     * @dataProvider providerStartsWith
     */
    public function testStartsWith($haystack, $needle, $expected)
    {
        $this->assertSame($expected, Str::startsWith($haystack, $needle));
    }

    /**
     * @covers ::trim
     * @dataProvider providerTrimArray
     */
    public function testTrimArray($str, $toTrim, $expected)
    {
        $this->assertSame($expected, Str::trim($str, $toTrim));
    }

    /**
     * @covers ::trim
     * @dataProvider providerTrimSimple
     */
    public function testTrimSimple($str, $toTrim, $expected)
    {
        $this->assertSame($expected, Str::trim($str, $toTrim));
    }

    public function providerTestContains()
    {
        return [
            ['abc', 'b', true],
            ['', 'b', false],
            ['abc', 'bc', true],
            ['abc', 'cb', false],
            ['abc', '', true],
            ['abc', 'A', false],
        ];
    }

    public function providerEndsWith()
    {
        return [
            ['abc', 'c', true],
            ['abc', '', true],
            ['abc', 'bc', true],
            ['abc', 'abc', true],
            ['abc', '0abc', false],
            ['abc', '', true],
        ];
    }

    public function providerStartsWith()
    {
        return [
            ['abc', 'a', true],
            ['abc', '', true],
            ['abc', 'ab', true],
            ['abc', 'abc', true],
            ['abc', 'abc0', false],
            ['abc', '', true],
        ];
    }

    public function providerTrimSimple()
    {
        return [
            ['abcdef', 'abfe', 'cd'],
            ['aacbeb', 'abf', 'cbe'],
        ];
    }

    public function providerTrimArray()
    {
        return [
            ['AAbbCCddEEEffGG', ['A', 'GG', 'f', 'EE'], 'bbCCddE'],
            ['A BBB C', ['A', 'C'], ' BBB '],
            [
                'A BBB C',
                array_merge(
                    str_split(Str::WHITESPACE_CHARS),
                    ['A', 'C', ' ']
                ),
                'BBB',
            ],
        ];
    }
}
