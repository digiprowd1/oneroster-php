<?php

namespace OneRosterTests\Internal;

use OneRoster\Internal\Uri;
use OneRosterTests\TestCase;

/**
 * @coversDefaultClass OneRoster\Internal\Uri
 */
class UriTest extends TestCase
{
    /**
     * @covers ::getPathParts
     * @dataProvider providerGetPathParts
     */
    public function testGetPathParts($uri, $expected)
    {
        $this->assertSame($expected, Uri::getPathParts($uri));
    }

    /**
     * @covers ::getQueryString
     * @dataProvider providerGetQueryString
     */
    public function testGetQueryString($uri, $expected)
    {
        $this->assertSame($expected, Uri::getQueryString($uri));
    }

    /**
     * @covers ::isDirectSubpath
     * @dataProvider providerIsDirectSubpathFail
     */
    public function testIsDirectSubpathFail($parent, $child)
    {
        $this->assertFalse(Uri::isDirectSubpath($parent, $child));
    }

    /**
     * @covers ::isDirectSubpath
     * @dataProvider providerIsDirectSubpathSuccess
     */
    public function testIsDirectSubpathSuccess($parent, $child)
    {
        $this->assertTrue(Uri::isDirectSubpath($parent, $child));
    }

    /**
     * @covers ::isSamePath
     * @dataProvider providerIsSamePathFail
     */
    public function testIsSamePathFail($path1, $path2)
    {
        $this->assertFalse(Uri::isSamePath($path1, $path2));
    }

    /**
     * @covers ::isSamePath
     * @dataProvider providerIsSamePathSuccess
     */
    public function testIsSamePathSuccess($path1, $path2)
    {
        $this->assertTrue(Uri::isSamePath($path1, $path2));
    }

    public function providerGetPathParts()
    {
        return [
            ['/a?b', ['a']],
            ['/a/b', ['a', 'b']],
            ['a/b/', ['a', 'b']],
        ];
    }

    public function providerGetQueryString()
    {
        return [
            ['/a?b', 'b'],
            ['/a?b=c', 'b=c'],
            ['/a', ''],
            ['/a?', ''],
        ];
    }

    public function providerIsDirectSubpathFail()
    {
        return [
            ['/a', '/b'],
            ['a', 'b'],
            ['/a/b', '/a/b/c/d'],
            ['/a/b', '/a/c'],
            ['/a/b', '/c/d'],
        ];
    }

    public function providerIsDirectSubpathSuccess()
    {
        return [
            ['/', '/b'],
            ['a', 'a/b'],
            ['a', '/a/b'],
            ['/a/b', '/a/b/c'],
            ['/a/b/', '/a/b/c'],
        ];
    }

    public function providerIsSamePathFail()
    {
        return [
            ['/a', '/b'],
            ['a', 'b'],
        ];
    }

    public function providerIsSamePathSuccess()
    {
        return [
            ['', '/'],
            ['a', '/a'],
            ['a', 'a/'],
            ['/a', '/a?param=1'],
            ['/a?param=1', '/a'],
        ];
    }
}
