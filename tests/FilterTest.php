<?php

namespace OneRosterTests;

use Carbon\Carbon;
use OneRoster\Filter;

class FilterTest extends TestCase
{
    /**
     * @dataProvider providerTestToString
     */
    public function testToString($filters, $expected)
    {
        $filterSet = $this->make();
        foreach ($filters as $filter) {
            call_user_func_array([$filterSet, 'add'], $filter);
        }

        $this->assertSame($expected, (string) $filterSet);
    }

    public function providerTestToString()
    {
        return [
            'single filter' => [
                [
                    ['a', '=', 1],
                ],
                "a='1'",
            ],
            'two filters' => [
                [
                    ['a', '=', 1],
                    ['b', '!=', 2],
                ],
                "a='1' AND b!='2'",
            ],
            'three filters' => [
                [
                    ['a', '=', 1],
                    ['b', '!=', 2, 'or'],
                    ['c', '<', 3, 'and'],
                ],
                "a='1' OR b!='2' AND c<'3'",
            ],
            'datetime' => [
                [
                    ['a', '>', Carbon::create(2001, 2, 3, 4, 5, 6)],
                ],
                "a>'2001-02-03T04%3A05%3A06.000Z'",
            ],
            'boolean' => [
                [
                    ['a', '=', true],
                ],
                "a='true'",
            ],
            'special char string' => [
                [
                    ['a', '=', 'how are things!?'],
                ],
                "a='how%20are%20things%21%3F'",
            ],
            'mixed case propert' => [
                [
                    ['aB', '=', 1],
                ],
                "aB='1'",
            ],
        ];
    }

    private function make()
    {
        return new Filter();
    }
}
