<?php

namespace OneRoster\Exceptions;

use UnexpectedValueException;

class MultiLinkException extends UnexpectedValueException
{
}
