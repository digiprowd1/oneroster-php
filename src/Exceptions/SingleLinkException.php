<?php

namespace OneRoster\Exceptions;

use UnexpectedValueException;

class SingleLinkException extends UnexpectedValueException
{
}
