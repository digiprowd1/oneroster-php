<?php

namespace OneRoster\Exceptions;

class UnexpectedResponseException extends ApiRequestException
{
}
