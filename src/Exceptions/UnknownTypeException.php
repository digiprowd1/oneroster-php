<?php

namespace OneRoster\Exceptions;

class UnknownTypeException extends Exception
{
}
