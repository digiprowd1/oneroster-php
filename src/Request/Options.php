<?php

namespace OneRoster\Request;

use InvalidArgumentException;

class Options
{
    private $appId = null;
    private $autoRetry = false;
    private $bearerToken = null;
    private $fields = null;

    public function getAppId()
    {
        return $this->appId;
    }

    public function getAutoRetry()
    {
        return $this->autoRetry;
    }

    public function getBearerToken()
    {
        return $this->bearerToken;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function loadQueryParameters($query)
    {
        // No query parameters are set by this class, only child classes.
    }

    public function loadQueryString($query)
    {
        $parameters = [];
        parse_str($query, $parameters);

        $this->loadQueryParameters($parameters);
    }

    public function setAppId($appId)
    {
        if ($appId !== null) {
            if (!is_string($appId)) {
                throw new InvalidArgumentException('$appId must be a string');
            }

            $length = strlen($appId);

            // Do some sanity checks on the app id
            // App ID's from ClassLink are 12 bytes, but URL-encoded (%XX), so have a max of 36 characters.
            // Use 100 for a good buffer.
            if ($length < 4) {
                throw new InvalidArgumentException('$appId too short');
            }

            if ($length > 100) {
                throw new InvalidArgumentException('$appId too long');
            }
        }

        $this->appId = $appId;
    }

    public function setAutoRetry($autoRetry)
    {
        if ($autoRetry === true) {
            $autoRetry = 3;
        } elseif ($autoRetry === false || $autoRetry === null) {
            $autoRetry = 0;
        } elseif (!is_int($autoRetry)) {
            throw new InvalidArgumentException('$autoRetry must be a non-negative int or bool');
        }

        $this->autoRetry = $autoRetry;

        return $this;
    }

    public function setBearerToken($bearerToken)
    {
        if ($bearerToken !== null) {
            if (!is_string($bearerToken) || !trim($bearerToken)) {
                throw new InvalidArgumentException('$bearerToken must be non-empty string or null');
            }
        }

        $this->bearerToken = $bearerToken;

        return $this;
    }

    public function setFields($fields)
    {
        if ($fields !== null) {
            if (is_string($fields)) {
                $fields = [$fields];
            } elseif (!is_array($fields)) {
                throw new InvalidArgumentException('$fields must be array of strings');
            }

            foreach ($fields as &$field) {
                if (!is_string($field)) {
                    throw new InvalidArgumentException('$fields must be array of strings');
                }

                $field = trim($field);
            }

            $fields = array_filter($fields);
        }

        if ($fields) {
            $this->fields = $fields;
        } else {
            $this->fields = null;
        }

        return $this;
    }

    public function toQueryParameters()
    {
        $params = [];

        $value = $this->getFields();
        if ($value) {
            $params['fields'] = implode(', ', $value);
        }

        return $params;
    }

    public static function fromQueryString($query)
    {
        $options = static::make();
        $options->loadQueryString($query);

        return $options;
    }

    public static function make()
    {
        return new static();
    }
}
