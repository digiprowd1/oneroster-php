<?php

namespace OneRoster\Request;

final class DefaultApiRequestor
{
    private static $requestor = null;

    private function __construct()
    {
    }

    public static function get(ApiRequestor $override = null)
    {
        if ($override) {
            return $override;
        }

        if (!static::$requestor) {
            static::$requestor = static::make();
        }

        return static::$requestor;
    }

    public static function set(ApiRequestor $requestor)
    {
        static::$requestor = $requestor;
    }

    private static function make()
    {
        $requestor = new GuzzleApiRequestor();

        return $requestor;
    }
}
