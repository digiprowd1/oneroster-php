<?php

namespace OneRoster;

class Course extends ApiResource
{
    public function getCourseCode()
    {
        return $this->get('courseCode');
    }

    public function getGrade()
    {
        return $this->grade;
    }

    public function getLinkRelations()
    {
        return [
            'org',
            'schoolYear',
        ];
    }

    public function getOrg()
    {
        return $this->getLinkTry('org');
    }

    public function getResourceType()
    {
        return 'course';
    }

    public function getSchoolYear()
    {
        return $this->getLinkTry('schoolYear');
    }

    public function getSubjects()
    {
        return $this->subjects ?: [];
    }

    public function getTitle()
    {
        return $this->get('title');
    }
}
