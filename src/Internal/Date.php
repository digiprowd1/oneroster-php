<?php

namespace OneRoster\Internal;

use Carbon\Carbon;
use DateTime;

class Date
{
    const FORMAT = 'Y-m-d\\TH:i:s.u\\Z';

    private function __construct()
    {
    }

    public static function format(DateTime $date)
    {
        if (!($date instanceof Carbon)) {
            $date = Carbon::instance($date);
        }

        $formatted = $date->format(static::FORMAT);

        $stripped = substr_replace($formatted, '', -4, 3);

        return $stripped;
    }

    public static function parseDate($dateStr)
    {
        $result = Carbon::parse($dateStr);
        $result->setTime(0, 0, 0);

        return $result;
    }

    public static function parseDateTime($dateTimeStr)
    {
        return Carbon::createFromFormat(static::FORMAT, $dateTimeStr);
    }
}
