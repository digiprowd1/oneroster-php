<?php

namespace OneRoster\Internal;

use GuzzleHttp\Psr7\Uri as GuzzleUri;

class Uri
{
    private function __construct()
    {
        // Static methods only
    }

    public static function getPathParts($uri)
    {
        $uri = new GuzzleUri($uri);

        return static::splitPath($uri);
    }

    public static function getQueryString($uri)
    {
        $uri = new GuzzleUri($uri);

        return $uri->getQuery();
    }

    public static function isDirectSubpath($parentUri, $childUri)
    {
        $parentUri = new GuzzleUri($parentUri);
        $childUri = new GuzzleUri($childUri);

        $parentPathParts = static::splitPath($parentUri);
        $childPathParts = static::splitPath($childUri);

        if (count($parentPathParts) + 1 !== count($childPathParts)) {
            return false;
        }

        return static::comparePathParts($parentPathParts, $childPathParts);
    }

    public static function isSamePath($uri1, $uri2)
    {
        $uri1 = new GuzzleUri($uri1);
        $uri2 = new GuzzleUri($uri2);

        $uri1Parts = static::splitPath($uri1);
        $uri2Parts = static::splitPath($uri2);

        if (count($uri1Parts) !== count($uri2Parts)) {
            return false;
        }

        return static::comparePathParts($uri1Parts, $uri2Parts);
    }

    private static function comparePathParts($parts1, $parts2)
    {
        $len1 = count($parts1);
        $len2 = count($parts2);

        for ($i = 0; $i < $len1 && $i < $len2; ++$i) {
            if ($parts1[$i] !== $parts2[$i]) {
                return false;
            }
        }

        return true;
    }

    private static function splitPath($path)
    {
        if ($path instanceof GuzzleUri) {
            $path = $path->getPath();
        }

        $parts = explode('/', $path);
        if (count($parts) && $parts[0] === '') {
            array_shift($parts);
        }

        if (count($parts) && $parts[count($parts) - 1] === '') {
            array_pop($parts);
        }

        return $parts;
    }
}
