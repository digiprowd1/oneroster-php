<?php

namespace OneRoster\Internal;

use OneRoster\ApiObject;

abstract class AbstractList extends ApiObject
{
    public function getFirst()
    {
        $items = $this->getItems();
        if (count($items)) {
            return $items[0];
        }

        return null;
    }

    public function getLast()
    {
        $items = $this->getItems();
        $count = count($items);
        if ($count) {
            return $items[$count - 1];
        }

        return null;
    }

    public function getItems()
    {
        return $this->getArrayToIterate();
    }
}
