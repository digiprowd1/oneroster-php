<?php

namespace OneRoster;

class User extends ApiResource
{
    const ROLE_TEACHER = 'teacher';
    const ROLE_STUDENT = 'student';
    const ROLE_PARENT = 'parent';
    const ROLE_GUARDIAN = 'guardian';
    const ROLE_RELATIVE = 'relative';
    const ROLE_AIDE = 'aide';
    const ROLE_ADMINISTRATOR = 'administrator';

    public function getAgents()
    {
        return $this->getLinkTry('agents') ?: [];
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getFamilyName()
    {
        return $this->get('familyName');
    }

    public function getGivenName()
    {
        return $this->get('givenName');
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function getLinkRelations()
    {
        return [
            'agents',
            'demographics',
            'orgs',
        ];
    }

    public function getName()
    {
        return new Name([
            'first' => $this->getGivenName(),
            'last' => $this->getFamilyName(),
        ]);
    }

    public function getOrgs()
    {
        return $this->getLink('orgs');
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getResourceType()
    {
        return 'user';
    }

    public function getRole()
    {
        return $this->get('role');
    }

    public function getSms()
    {
        return $this->sms;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public static function getRoles()
    {
        return [
            static::ROLE_TEACHER,
            static::ROLE_STUDENT,
            static::ROLE_PARENT,
            static::ROLE_GUARDIAN,
            static::ROLE_RELATIVE,
            static::ROLE_AIDE,
            static::ROLE_ADMINISTRATOR,
        ];
    }
}
