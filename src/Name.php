<?php

namespace OneRoster;

use OneRoster\Internal\Str;

class Name extends ApiObject
{
    public function getFormalShort()
    {
        return $this->format('L, F');
    }

    public function getFullShort()
    {
        return $this->format('F L');
    }

    protected function format($str)
    {
        $replacements = [
            'F' => $this->first ?: '',
            'L' => $this->last ?: '',
        ];

        $str = strtr($str, $replacements);
        $whitespace = str_split(Str::WHITESPACE_CHARS);
        $toTrim = array_merge($whitespace, [
            ',',
        ]);

        $str = Str::trim($str, $toTrim);
        $str = preg_replace('/\s+/', ' ', $str);

        return $str;
    }
}
