<?php

namespace OneRoster;

use Carbon\Carbon;
use DateTime;
use InvalidArgumentException;
use OneRoster\Internal\Date;

class Filter
{
    protected $filters = [];
    protected $raw = null;

    public function __toString()
    {
        return $this->toString();
    }

    public function add($property, $predicate, $value, $logicalOperator = 'and')
    {
        if (!preg_match('/^[a-z0-9]+(\.[a-z0-9]+)*$/i', $property)) {
            throw new InvalidArgumentException('$property invalid');
        }

        if (!in_array($predicate, $this->getPredicates(), true)) {
            throw new InvalidArgumentException('$predicate invalid');
        }

        try {
            $this->stringifyValue($value);
        } catch (InvalidArgumentException $e) {
            throw new InvalidArgumentException('$value invalid');
        }

        if (!in_array($logicalOperator, $this->getLogicalOperators(), true)) {
            throw new InvalidArgumentException('$logicalOperator invalid');
        }

        $filter = [
            'property' => $property,
            'predicate' => $predicate,
            'value' => $value,
            'logical' => $logicalOperator,
        ];

        $this->filters[] = $filter;
    }

    public function isEmpty()
    {
        if ($this->raw) {
            return false;
        }

        return !count($this->filters);
    }

    public function toString()
    {
        if ($this->raw) {
            return $this->raw;
        }

        $strings = [];
        for ($i = 0; $i < count($this->filters); ++$i) {
            $stringified = '';
            $filter = $this->filters[$i];

            if ($i > 0) {
                $stringified .= strtoupper($filter['logical']) . ' ';
            }

            $stringified .= $filter['property'];
            $stringified .= $filter['predicate'];

            $value = $this->stringifyValue($filter['value']);
            $value = rawurlencode($value);

            $stringified .= '\'' . $value . '\'';

            $strings[] = $stringified;
        }

        return implode(' ', $strings);
    }

    public static function getLogicalOperators()
    {
        return [
            'and',
            'or',
        ];
    }

    public static function getPredicates()
    {
        return [
            '=',
            '!=',
            '>',
            '>=',
            '<',
            '<=',
            // 'contains', // In spec, but how does this get encoded?
        ];
    }

    public static function make($property, $predicate, $value)
    {
        $filter = new static();
        $filter->add($property, $predicate, $value);

        return $filter;
    }

    public static function raw($rawFilter)
    {
        $filter = new static();
        $filter->raw = $rawFilter;

        return $filter;
    }

    protected function stringifyValue($value)
    {
        if (is_string($value)) {
            return $value;
        }

        if (is_bool($value)) {
            return $value ? 'true' : 'false';
        }

        if (is_numeric($value)) {
            return (string) $value;
        }

        if ($value instanceof DateTime) {
            $value = Carbon::instance($value);
            $value->timezone = 'UTC';

            return Date::format($value);
        }

        throw new InvalidArgumentException('$value not serializable');
    }
}
