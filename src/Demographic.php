<?php

namespace OneRoster;

class Demographic extends ApiResource
{
    public function getIndexType()
    {
        return $this->getResourceType();
    }

    public function getResourceType()
    {
        return 'demographics';
    }
}
